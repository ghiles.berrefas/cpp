#include <iostream>
//exo2 
// 1. Ecrire une fonction valAt qui retourne une référence sur une valeur entière qui est la
// valeur située dans un tableau d'entiers tab à l'indice i. tab et i sont fournis en
// paramètres de la fonction valAt.

int & valAt(int tab[], int i){
    return *(tab+i) ; // return tab[i]
}

// 2. Ecrire un programme qui affiche le contenu d'un tableau d'entiers en utilisant la
// fonction valAt.

int main() {

    std::cout << "start_test_Point" << std::endl;
    int tab[] = {4,5,8,7,10};

    for(int i =0 ; i < 5; i++) {
        int& ref(valAt(tab,i)) ; 
        std::cout << " i = " << i << " | ref = " << ref << std::endl; 
        // std::cout << " i = " << i << " | ref = " << valAt(tab,i) << std::endl; 
    }
    std::cout << "end_test_Point" << std::endl;

}