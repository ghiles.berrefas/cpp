#include <iostream>
#include "Complex.h"

// Ecrire la classe complex, avec ses constructeurs par défaut, avec arguments (par
// défaut, créer le complexe 0+0i) et son destructeur.
// 2. Toutes ces méthodes doivent afficher un message indiquant qu'elles sont en cours
// d'exécution.
// 3. Modifier le constructeur avec arguments afin qu'il ait deux arguments par défaut
// (valant 0). Le constructeur par défaut est-il toujours nécessaire ?
// 4. Tester la classe Complex dans le fichier main.cpp.
// 5. Ajouter à la classe complex un constructeur par recopie affichant un message, puis
// ajouter au programme principal une instruction permettant l'appel de ce constructeur
// par recopie.
//Le constructeur par défaut est-il toujours nécessaire ? 
// faudrais a evité d'avoi deux constructeur par default .... 

Complex::Complex(double _r,double _im) : r(_r),im(_im) {
    std::cout << "constructeur avec arg en cours d'exe"<< std::endl ; 
}

Complex::Complex(const Complex & c){
    r = c.r ;
    im = c.im ;
    std::cout << "constructeur par recopie en cours d'exe"<< std::endl ; 
}

Complex::~Complex(){
    std::cout << "deconstructeur par defaut en cours d'exe"<< std::endl ;
}

Complex& Complex::operator=(const Complex & c){
    std::cout << "Par operator" << std::endl;
    if (this == &c)
        return *this;
    this->r = c.r;
    this->im = c.im;
    return *this;
}

int main(){

    std::cout << "start_test_Complex................" << std::endl;
    Complex c(45,5); 
    Complex c1; 
    Complex c4 = c;
    Complex c5; 
    c5 = c; 
    std::cout << "end_..................." << std::endl; 

}