#include <iostream>
#include "Test.h"


//exo1
// Ecrire une fonction traitement qui prend en paramètre une référence constante sur
// un objet de type Test et qui fait appel à la méthode increment et print. Faites le test
// dans le main. Que se passe-t-il à la compilation? corrigez le code

void traitement(Test & t){ // supprime le const pour acceder au methode en l'ecture | modifie l'instance apres l'appel a la fonction 
    t.increment(5);
    t.print(); 
}

//exo2 
// 1. Ecrire une fonction valAt qui retourne une référence sur une valeur entière qui est la
// valeur située dans un tableau d'entiers tab à l'indice i. tab et i sont fournis en
// paramètres de la fonction valAt.

int & valAt(int tab[], int i){
    return *(tab+i) ; // return tab[i]
}

// 2. Ecrire un programme qui affiche le contenu d'un tableau d'entiers en utilisant la
// fonction valAt.

int main() {
    // Test t = Test(); 
    // traitement(t);
    std::cout << "start_test_Point" << std::endl;
    int tab[] = {4,5,8,7,10};

    for(int i =0 ; i < 5; i++) {
        int& ref(valAt(tab,i)) ; 
        std::cout << " i = " << i << " | ref = " << ref << std::endl; 
    }
    std::cout << "end_test_Point" << std::endl;

}