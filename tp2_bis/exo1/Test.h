#include <iostream>

class Test{

    private :
        long a;
    public :
        Test() { a = 0; }
        ~Test() {}
        void increment(long n) { a = a + n; }
        void print() const { std::cout << "a = " << a << std::endl; }
        // void traitement(Test  & t) ;
};

