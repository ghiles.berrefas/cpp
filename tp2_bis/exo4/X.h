#include <iostream> 

class X
{
    private:
        long vals[10];
        long util;
    public:
        X();
        X(const X & );
        ~X();
        void print_vals() const ;
        void set_vals(int i,long new_value);
};