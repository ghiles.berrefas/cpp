#include <iostream>
#include "X.h"

// 1. Créez une classe X dont un des membres est un tableau alloué statiquement.
// 2. Créez deux objets a et b de classe X, en créant b à partir de a. Quel est le
// constructeur appelé pour créer b ?
// 3. Modifier un élément du tableau stocké dans a et affichez b. Qu'en déduire sur le
// constructeur utilisé pour créer b ?
// 4. Ecrivez votre propre version de ce constructeur et testez-la.
long & valAt(long tab[], int i){
    return *(tab+i) ; // return tab[i]
}

X::X(){
    std::cout << "C par defaut" << std::endl;
    for(int i=0; i<10;i++){
        this->vals[i] = 1 ; 
    }
}

X::X( const X & x){
    x.print_vals();
    std::cout << "C copie " <<  x.vals[5] << std::endl;
    for(int i=0; i < 10 ; i++){
        this->vals[i] = x.vals[i];
    }
}

void X::set_vals(int index , long new_value){
    valAt(this->vals,index) = new_value;
}

X::~X(){

}

void X::print_vals() const {
    for(int i = 0 ; i<10 ; i++){
        std::cout << this->vals[i] << " | " ;
    } 
    std::cout << std::endl ;
}

int main(){
    std::cout << "start_test_Complex................" << std::endl;
    X a; 
    X b = a; 
    a.print_vals();
    a.set_vals(5,25);
    a.print_vals();
    b.print_vals();
    std::cout << "end_..................."  << std::endl; 
}
