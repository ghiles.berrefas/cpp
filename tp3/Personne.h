#include <iostream> 

class Date {

    private:
        int jour;
        int mois;
        int annee;

    public:
        Date();
        Date(int jour,int mois,int annee);
        ~Date();
        int getJour()const{ return jour; }
        int getMois()const{ return mois; }
        int getannee()const{ return annee; }
        void setJour(int j){ jour = j; }
        void setMois(int m){ mois = m; }
        void setAnnee(int a){ annee = a; }
};

class Personne {
//     Ajoutez les attributs et méthodes nécessaires pour pouvoir marier deux instances existantes
// de la classe Personne et éventuellement leur permettre de changer de conjoint ou de
// redevenir célibataires !
// Doit-on supprimer l’attribut conjoint dans le destructeur lorsqu’une instance de la classe
// Personne est supprimée ? Expliquez en quoi c’est différent de ce qui est fait pour l’instance
// de la classe Date (qui représente la date de naissance).
    private:
        std::string nom;
        Date* ddn; 
        Personne* conjoint;

    public:
        Personne();
        Personne(std::string nom, int j, int m, int a);
        ~Personne();  
        std::string getNom() const;
        int getAge() const;
        void est_marier_a(Personne* a);
};

