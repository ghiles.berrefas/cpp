#include <iostream> 
#include <ctime>
#include "Personne.h"

const int OFFSET_YEAR =1900 ;

Date::Date(){
    time_t t = time(NULL);
    tm* timeptr = localtime(&t);
    jour = timeptr->tm_mday;
    mois = timeptr->tm_mon;
    annee = timeptr->tm_year+OFFSET_YEAR;
}

Date::Date(int j,int m,int a)
    :jour(j),mois(m),annee(a){
    std::cout << "Date::Date" << std::endl;
}

Date::~Date(){
    std::cout << "Date::~Date" << std::endl;
}

Personne::Personne(std::string nom, int j, int m, int a) {
    std::cout << "Personne::Personne" << std::endl;
    this->nom = nom;
    ddn = new Date(j,m,a);  
    conjoint = NULL;
}

std::string Personne::getNom()const{
    return this->nom;
}
/*oui il est nécessaire d'implémenter un destructeur pour la class personne car dans le contructeur
on a alluer dynamiquement une instance d'une Date, qu'on devra librer dans le destructeur pour eviter 
une fuite memoire.
a noté que si le destructeur de la Date n'ai pas declarer explicitement 
l'erreur ce produit : 
Personne.cpp:(.text+0x208): undefined reference to `Date::~Date()'
collect2: error: ld returned 1 exit status
*/
Personne::~Personne(){
    std::cout << "Personne::~Personne" << std::endl;
    delete ddn; 
    // delete conjoint;
}

int Personne::getAge()const{
    time_t t = time(NULL);
	tm* timePtr = localtime(&t);
    return (timePtr->tm_year+OFFSET_YEAR) - ddn->getannee();
}

void Personne::est_marier_a(Personne* a){ 
    conjoint = a ;
    if (conjoint != NULL)
        std::cout << getNom() << " est_marier_a " << conjoint->getNom() << std::endl;
    else 
        std::cout << getNom() << "est célibataires" << std::endl;
}

int main (){
    Personne phil("Philippe Durant",13,02,1998);
    // Personne alice = Personne("Alice tata",15,20,2000);
    Personne* alice = new Personne("Alice tata",15,20,2000);
    std::cout << phil.getNom() << std::endl;
    std::cout << phil.getAge() << std::endl;
    // phil.est_marier_a(&alice);
    // phil.est_marier_a(NULL);
    phil.est_marier_a(alice);


}

 