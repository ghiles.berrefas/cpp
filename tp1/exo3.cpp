#include <iostream>

using namespace std;

void enter_two_strings(){
    string s1 ;
    string s2 ;
    cout << "Entrez une 1er chaine de caractères" << endl ;
    cin >> s1 ;
    cout << "Entrez une 2eme chaine de caractères" << endl ;
    cin >> s2 ;
    string s3 = s1 + "_" + s2 ;
    cout << "Voici la concatenation des deux chaines: " << s3 << endl;
    return;
}

void devise_string_by_espace(string S,int len_S, string & s1, string & s2){
    cout << "s1: " << s1 << endl;
    cout << "s2: " << s2 << endl;
    bool find_espace = false;
    int j =0;
    for (int i =0 ; i < len_S ; i++){
        if (S[i] == ' ' ){
            find_espace = true;
            continue;
        }
        if(!find_espace){
            //cout << S[i] << endl;
            s1[i]= S[i] ;
            cout << s1[i] << " s1 = " << s1 << endl ;
        }else{
            s2[j]=S[i] ;
            j++;
        }
    }
    //s1 = "ghiles";
    cout << "s1: " << s1 << endl;
    cout << "s2: " << s2 << endl;
}

void enter_firts_and_last_name(){
    
    string S ; 
    int index_espace;
    do{
        cout << "Entrez votre nom et prenom separer par un espace 'nom prenom'" << endl;
        getline(cin,S) ;
        cout << "S: " << S << endl;
        index_espace = S.find(" ");
        if( index_espace != -1){
            string s1 = S.substr(0,index_espace);
            string s2 = S.substr(index_espace,S.size()-1);
            cout << "Bonjour, votre nom est " << s1 << " et votre prénom est " << s2 << endl;
        }else{
            cout << "Veuillez s'il vous plait enter un format 'nom prenom' " << endl;
        }
    }while(index_espace == -1) ;
    
}

int nbr_apparitions_car(const  string & s, char c){

    int cp = 0;
    for(char car : s){
        if (car == c){
            cp++;
        }
    }
    // for(int i=0; i < (int) s.size(); i++){
    //     if (s[i]== c){
    //         cp++;
    //     }
    // }
    return cp;
}

void replace_car(string & s, char old_c, char new_c){
    for(int i=0; i < (int) s.size(); i++){
        if (s[i]== old_c){
            s[i]=new_c;
        }
    }
}

int main(int argc, char **argv){
    // 1
    // enter_two_strings();
    // 2
    // enter_firts_and_last_name();
    // 3 
    string s ("BERREFAS");
    char c('R');
    cout << "nbr_apparitions_car " << c << endl << nbr_apparitions_car(s, c) << endl;
    // 4   
    // replace_car(s,c,'V');
    // cout << "AFTER : " << s << endl ;
    
    return EXIT_SUCCESS;
}