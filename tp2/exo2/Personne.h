
#include <iostream> 

class Personne {
    
    private:
        std::string first_name;
        std::string last_name;
        int age; 
        static int nb_instance ;

    public:
        // On veut pouvoir créer des instances soit en spécifiant le nom, le prénom et l'âge, soit
        // en ne spécifiant rien. On suppose qu’une Personne est d’âge 18 ans par défaut.
        // Définissez les constructeurs et destructeur associés.
        Personne(std::string first_name, std::string last_name , int age = 18 );
        Personne(const Personne &);
        ~Personne() ; // class A  | Class B 
        std::string get_first_name() const;
        std::string get_last_name() const;
        int get_age() const;
        void set_first_name(std::string new_first_name);
        void set_last_name(std::string new_last_name);
        void set_age(int age);
        void print_personne() const ;
        static int getNmembersinstances();//methode static accedé directement depuis 
        // virtual  decloncher la liason dynamique  
};

