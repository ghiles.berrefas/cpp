#include <iostream> 
#include "Personne.h"

int Personne::nb_instance = 0 ;

Personne::Personne(std::string _first_name, std::string _last_name , int _age) : 
                first_name(_first_name), last_name(_last_name) , age(_age) {
    std::cout << "Constructeur avec param" << std::endl;
    Personne::nb_instance++;
};

Personne::Personne(const Personne & p){
    this->age = p.age;
    this->first_name = p.get_first_name();
    this->last_name = p.get_last_name();   
    Personne::nb_instance++;
}

std::string Personne::get_first_name()const{
    return this->first_name;
}

std::string Personne::get_last_name()const{
    return this->last_name;
}

int Personne::get_age()const{
    return this->age;
}

void Personne::set_first_name(std::string new_first_name){
    this->first_name = new_first_name; 
}

void Personne::set_last_name(std::string new_last_name){
    this->last_name = new_last_name;  
}

void Personne::set_age(int age){
    this->age = age ;
}

void Personne::print_personne() const { 
    std::cout << first_name << "," << last_name <<"has"<< age << "old year" << std::endl ;
}

Personne::~Personne(){

}

