// Ecrivez une classe Rectangle dont le constructeur prend deux paramètres, largeur et
// hauteur et qui offre les fonctions suivantes :
//  Calcul du périmètre
//  Calcul de la surface
//  Affichage
// On ne souhaite pas que l’utilisateur de cette classe puisse accéder directement à ses
// attributs. Proposez les mécanismes nécessaires pour assurer cette protection des données
// mais permettre quand même de consulter et de modifier ces valeurs (via des accesseurs et
// mutateurs).
// Imaginez un petit programme qui utilise cette classe.


class Rectangle {

    private : 
        int width ; 
        int height ;

    public : 
        Rectangle(int width, int height);
        int perimeter();
        int area();
        void print_Rectangle() ; 
};