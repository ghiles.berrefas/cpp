// Ecrivez une classe Rectangle dont le constructeur prend deux paramètres, largeur et
// hauteur et qui offre les fonctions suivantes :
//  Calcul du périmètre
//  Calcul de la surface
//  Affichage
// On ne souhaite pas que l’utilisateur de cette classe puisse accéder directement à ses
// attributs. Proposez les mécanismes nécessaires pour assurer cette protection des données
// mais permettre quand même de consulter et de modifier ces valeurs (via des accesseurs et
// mutateurs).
// Imaginez un petit programme qui utilise cette classe.
#include <iostream>
#include "Rectangle.h"

Rectangle::Rectangle(int _width , int _height) : width(_width),height(_height){}

int Rectangle::perimeter(){
    return 2*this->height + 2*this->width;
}

int Rectangle::area(){
    return this->height*this->width;
}

void Rectangle::print_Rectangle() {
    std::cout << "width:" << this->width << " | height:" << this->height; 
}