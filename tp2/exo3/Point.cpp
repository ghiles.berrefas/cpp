#include <iostream> 
#include <cmath>
#include "Point.h"

int Point::nbr_instances = 0 ;
int Point::nbreInstances() {
    return Point::nbr_instances ;
}

Point::Point(double x, double y): 
                x(x) , y(y) {
    std::cout << "contructeur point" << std::endl ;
    Point::nbr_instances++;
}

void Point::affiche() const {
    std::cout << "x = " << this->x << " | y= " << this->y << " | nbr_ins= " 
    << Point::nbreInstances() << std::endl;
}

void Point::deplacer(double dx, double dy) { 
    this->x = this->x + dx;
    this->y = this->y + dy;
}

double Point::get_x() const {
    return this->x;
}

double Point::get_y() const {
    return this->y;
}

void Point::positionner(const Point & p){
    this->x = p.get_x();
    this->y = p.get_y(); 
}

double Point::distance(const Point & p){
    return std::sqrt(std::pow(this->x-p.get_x(),2.0)+std::pow(this->y-p.get_y(),2.0));
}

Point::~Point() {
    
}

int main(){
    std::cout << "start_test_Point" << std::endl;
    Point p(1.2,5.0);
    Point* p1;  
    p1 = &p;
    p.affiche();



}