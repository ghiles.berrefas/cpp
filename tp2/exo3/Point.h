
#include <iostream> 

class Point {
    private:
        double x;
        double y;
        static int nbr_instances ; 
    public:
        Point(double x, double y);
        Point(const Point & );
        ~Point() ;
        Point & operator=(const Point & point) ; 
        void affiche() const ;
        void deplacer(double dx, double dy) ;
        void positionner(double x, double y) ;
        // const pour ne pas modifier le point passer en parametre p 
        // acces en lecteur uniquement 
        void positionner(const Point & p) ; 
        double distance(const Point & p); 
        double get_x() const;
        double get_y() const;
        static int nbreInstances();
};

